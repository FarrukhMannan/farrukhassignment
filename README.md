
# Terraform Assignment



Deployment of Load balancer with ec2 instances

Assign Date :     10-5-22
Completed Date : 12-5-22

Lookup:
lookup retrieves the value of a single element from a map, given its key. If the given key does not exist, the given default value is returned instead.

For each:
for_each is a meta-argument defined by the Terraform language. It can be used with modules and with every resource type. The for_each meta-argument accepts a map or a set of strings, and creates an instance for each item in that map or set.

Count:
The count meta-argument accepts a whole number and creates the number of instances of the resource specified.

Dynamic function:
A dynamic block acts much like a for expression, but produces nested blocks instead of a complex typed value. It iterates over a given complex value, and generates a nested block for each element of that complex value.


```bash

######################################################

#Solution:

#Main files Root module

#provider.tfvars

provider "aws" {
  region     = "${var.region}"
   access_key = "${var.access_key}"
   secret_key = "${var.secret_key}"
}  
######################################################

#terraform.tfvars
 

subnets_cidr  = ["10.20.1.0/24", "10.20.2.0/24"]
azs           = ["us-east-2a", "us-east-2b"]
servernames   = ["farrukhserver1", "farrukhserver2"]

######################################################

#Variable.tf
variable "access_key" {
  type = string
}

variable "secret_key" {
  type = string
}

variable "region" {
  
  type = string
  description = "aws_region"
  default = "us-east-2"

}

variable "ami" {
  #type = string
  default = {
    
        "us-east-2"= "ami-0aeb7c931a5a61206"
        "us-east-1"="ami-09d56f8956ab235b3"
  
      }
}

variable "instance_type" {
  #type = string
  default = {
        "us-east-2"="t2.micro"
        "us-east-1"="t2.micro"
  
      }
}


variable "vpc_cidr" {
  default = "10.20.0.0/16"
}

variable "subnets_cidr" {
  type = list(any)
}

variable "azs" {
  type = list(any)
}


variable "servernames" {
  type = list(string)

}


 
 
######################################################


#Output.tf
output "vpc_id" {
    value = module.vpc.vpc_id
}

output "subnet_id" {
    value =  module.vpc.subnet.*.id
    }



 
 ######################################################


#vpcmain.tf



# VPC
resource "aws_vpc" "terra_vpc" {
  cidr_block = var.vpc_cidr
  tags = {
    Name = "farrukhVPC"
  }
}

# Internet Gateway
resource "aws_internet_gateway" "terra_igw" {
  vpc_id = aws_vpc.terra_vpc.id
  tags = {
    Name = "FarrukhIGW"
  }
}

# Subnets : public
resource "aws_subnet" "public" {
  count                   = length(var.subnets_cidr)
  vpc_id                  = aws_vpc.terra_vpc.id
  cidr_block              = element(var.subnets_cidr, count.index)
  availability_zone       = element(var.azs, count.index)
  map_public_ip_on_launch = true
  tags = {
    Name = "Subnet-${count.index + 1}"
  }
}

# Route table: attach Internet Gateway 
resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.terra_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.terra_igw.id
  }
  tags = {
    Name = "publicRouteTable"
  }
}

# Route table association with public subnets
resource "aws_route_table_association" "a" {
  count          = length(var.subnets_cidr)
  subnet_id      = aws_subnet.public[count.index].id
  route_table_id = aws_route_table.public_rt.id
}


######################################################
#Vpcvariable.tf

variable "vpc_cidr" {
  default = "10.20.0.0/16"
}

variable "subnets_cidr" {
  type = list(any)
}

variable "azs" {
  type = list(any)
}

######################################################


#Vpcoutput.tf

output "vpc_id" {
    value = aws_vpc.terra_vpc.id
}

output "subnet" {
    value = aws_subnet.public
}

######################################################
#module/webserver

#Webservermain.tf

 
########################################################
 
 
#keypair
 
resource "aws_key_pair" "kp-tf" {
 key_name   = "kps-key"
 public_key = var.key
}
 
########################################################
 
# Ec2 instance
 
resource "aws_instance" "webserver1" {
 count = length(var.name)
 ami             = var.ami
 instance_type   = var.instance_type
 key_name        = aws_key_pair.kp-tf.key_name
 subnet_id       = var.subnet_id[count.index]
 security_groups =  [aws_security_group.instancesg.id]
 user_data = <<EOF
#!/bin/bash
sudo apt-get update -y
sudo apt-get install apache2 -y
sudo systemctl start apache2
sudo echo “Hello World from $(hostname -f)” > /var/www/html/index.html
  
EOF
 
 
 tags = {
   Name = "${var.name[count.index]}"
 }
}
 
#########################################################
#security group for ec2 instances

resource "aws_security_group" "instancesg" {
 name        = "instancesg"
 description = "Allow "
 vpc_id      =  var.vpc_id
 
 
 
#  ingress {
#    description = "http"
#    from_port   = 22
#    to_port     = 22
#    protocol    = "tcp"
#    cidr_blocks = ["0.0.0.0/0"]
#  }
 
   dynamic "ingress" {
    for_each = [80]
    iterator=port
    content {
        description = "http"
        from_port   = port.value
        to_port     = port.value
        protocol    = "tcp"
        security_groups  = ["${aws_security_group.publicsg.id}"]
    }
  }






#  ingress {
#    description = "http"
#    from_port   = 80
#    to_port     = 80
#    protocol    = "tcp"
#    security_groups  = ["${aws_security_group.publicsg.id}"]

#  }
 
 
 egress {
   from_port   = 0
   to_port     = 0
   protocol    = "-1"
   cidr_blocks = ["0.0.0.0/0"]
 }
 
 
 tags = {
   Name = "farrukhinstancesg"
 }
}
 
#########################################################

#Security group -lb
 
resource "aws_security_group" "publicsg" {
 name        = "publicsg"
 description = "Allow "
 vpc_id      =  var.vpc_id
 
 dynamic "ingress" {
    for_each = [80]
    iterator=port
    content {
        description = "http"
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
  }
 

#  ingress {
#    description = "http"
#    from_port   = 80
#    to_port     = 80
#    protocol    = "tcp"
#    cidr_blocks = ["0.0.0.0/0"]
#  }
 
 
 egress {
   from_port   = 0
   to_port     = 0
   protocol    = "-1"
   cidr_blocks = ["0.0.0.0/0"]
 }
 
 
 tags = {
   Name = "farrukhlbdg"
 }
}






####################################################
# Target Group Creation
####################################################
 
resource "aws_lb_target_group" "tg" {
 name        = "TargetGroup"
 port        = 80
 target_type = "instance"
 protocol    = "HTTP"
 vpc_id      = var.vpc_id
}
 
####################################################
# Target Group Attachment with Instance
####################################################
 
resource "aws_alb_target_group_attachment" "tgattachment" {
 count            = length(aws_instance.webserver1.*.id)
 target_group_arn = aws_lb_target_group.tg.arn
 target_id        = element(aws_instance.webserver1.*.id, count.index)
}
 
####################################################
# Application Load balancer
####################################################
 
resource "aws_lb" "lb" {
 name               = "ALB"
 internal           = false
 load_balancer_type = "application"
 security_groups    = [aws_security_group.publicsg.id]
 subnets            = var.subnet_id
}
 
####################################################
# Listner Rule
####################################################
 
resource "aws_lb_listener" "front_end" {
 load_balancer_arn = aws_lb.lb.arn
 port              = "80"
 protocol          = "HTTP"
 
 default_action {
   type             = "forward"
   target_group_arn = aws_lb_target_group.tg.arn
 }
}

 
######################################################
#Webservervariable.tf
variable "ami" {
}

variable "instance_type" {
}

variable "name" {
  #type = list(string)

}

variable "subnet_id" {
  type = list(string)
}


variable "key" {
  type= string
  
}

variable "vpc_id" {
  type= string

}

######################################################
#Webserveroutput.tf
 

output "instance" {
  value = aws_instance.webserver1
}




```
Acknowledggements:

[How to attach one security group with other resource](https://stackoverflow.com/questions/35384657/allow-access-to-one-aws-security-group-to-another-using-terraform)

[How to make module and file structure](https://github.com/dasanirban834/Terraform-task/tree/main/AWS-infrastructure-with-alb)

[How to make module in Terraform](https://blog.gruntwork.io/how-to-create-reusable-infrastructure-with-terraform-modules-25526d65f73d)


[Terraform and AWS Application Load Balancers](https://medium.com/cognitoiq/terraform-and-aws-application-load-balancers-62a6f8592bcf)

[How to Deploy a Set of EC2 Instances behind an ALB Using Terraform](https://aws.plainenglish.io/deploy-a-set-of-ec2-instances-behind-an-alb-using-terraform-403fe584f09e)

[How to Deploy a Set of EC2 Instances Using Terraform](https://github.com/dasanirban834/Terraform-task/tree/main/AWS-infrastructure-with-alb)

