 
########################################################
 
 
#keypair
 
resource "aws_key_pair" "kp-tf" {
 key_name   = "kps90909-key"
 public_key = var.key
}
 
########################################################
 
# Ec2 instance
 
resource "aws_instance" "webserver1" {
 count = length(var.name)
 ami             = var.ami
 instance_type   = var.instance_type
 key_name        = aws_key_pair.kp-tf.key_name
 subnet_id       = var.subnet_id[count.index]
 security_groups = [aws_security_group.publicsg.id]
 user_data = <<EOF
#!/bin/bash
sudo apt-get update -y
sudo apt-get install apache2 -y
sudo systemctl start apache2
sudo echo “Hello World from $(hostname -f)” > /var/www/html/index.html
  
EOF
 
 
 tags = {
   Name = "${var.name[count.index]}"
 }
}
 
# #########################################################
#security group for ec2 instances

resource "aws_security_group" "publicsg" {
 name        = "instancesg"
 description = "Allow "
 vpc_id      =  var.vpc_id
 
 
 
 ingress {
   description = "http"
   from_port   = 80
   to_port     = 80
   protocol    = "tcp"
   cidr_blocks = ["0.0.0.0/0"]
 }
 
}
  #  dynamic "ingress" {
  #   for_each = [80]
  #   iterator=port
  #   content {
  #       description = "http"
  #       from_port   = port.value
  #       to_port     = port.value
  #       protocol    = "tcp"
  #       security_groups  = var.security_groups
  #   }
  # }






# #  ingress {
# #    description = "http"
# #    from_port   = 80
# #    to_port     = 80
# #    protocol    = "tcp"
# #    security_groups  = ["${aws_security_group.publicsg.id}"]

# #  }
 
 
#  egress {
#    from_port   = 0
#    to_port     = 0
#    protocol    = "-1"
#    cidr_blocks = ["0.0.0.0/0"]
#  }
 
 
#  tags = {
#    Name = "farrukhinstancesg"
#  }
# }
 
# #########################################################

# #Security group -lb
 
# resource "aws_security_group" "publicsg" {
#  name        = "publicsg"
#  description = "Allow "
#  vpc_id      =  var.vpc_id
 
#  dynamic "ingress" {
#     for_each = [80]
#     iterator=port
#     content {
#         description = "http"
#         from_port   = 80
#         to_port     = 80
#         protocol    = "tcp"
#         cidr_blocks = ["0.0.0.0/0"]
#     }
#   }
 

# #  ingress {
# #    description = "http"
# #    from_port   = 80
# #    to_port     = 80
# #    protocol    = "tcp"
# #    cidr_blocks = ["0.0.0.0/0"]
# #  }
 
 
#  egress {
#    from_port   = 0
#    to_port     = 0
#    protocol    = "-1"
#    cidr_blocks = ["0.0.0.0/0"]
#  }
 
 
#  tags = {
#    Name = "farrukhlbdg"
#  }
# }






# ####################################################
# # Target Group Creation
# ####################################################
 
# resource "aws_lb_target_group" "tg" {
#  name        = "TargetGroup"
#  port        = 80
#  target_type = "instance"
#  protocol    = "HTTP"
#  vpc_id      = var.vpc_id
# }
 
# ####################################################
# # Target Group Attachment with Instance
# ####################################################
 
# resource "aws_alb_target_group_attachment" "tgattachment" {
#  count            = length(aws_instance.webserver1.*.id)
#  target_group_arn = aws_lb_target_group.tg.arn
#  target_id        = element(aws_instance.webserver1.*.id, count.index)
# }
 
# ####################################################
# # Application Load balancer
# ####################################################
 
# resource "aws_lb" "lb" {
#  name               = "ALB"
#  internal           = false
#  load_balancer_type = "application"
#  security_groups    = [aws_security_group.publicsg.id]
#  subnets            = var.subnet_id
# }
 
# ####################################################
# # Listner Rule
# ####################################################
 
# resource "aws_lb_listener" "front_end" {
#  load_balancer_arn = aws_lb.lb.arn
#  port              = "80"
#  protocol          = "HTTP"
 
#  default_action {
#    type             = "forward"
#    target_group_arn = aws_lb_target_group.tg.arn
#  }
# }
