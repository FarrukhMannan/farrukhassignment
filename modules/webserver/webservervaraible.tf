variable "ami" {
  type = string
  # default = {
    
  #       "us-east-2"= "ami-0aeb7c931a5a61206"
  #       "us-east-1"="ami-09d56f8956ab235b3"
  
  #     }
}

variable "instance_type" {
  type = string
  # default = {
  #       "us-east-2"="t2.micro"
  #       "us-east-1"="t2.small"
  
  #     }
}

variable "name" {
  type = list(string)

}

variable "subnet_id" {
  type = list(string)
}


variable "key" {
  type= string
  
}

variable "vpc_id" {
  type= string

}

# variable "security_groups" {
#   # type= string

# }