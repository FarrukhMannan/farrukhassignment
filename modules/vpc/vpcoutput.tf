output "vpc_id" {
    value = aws_vpc.terra_vpc.id
}

output "subnet" {
    value = aws_subnet.public
}