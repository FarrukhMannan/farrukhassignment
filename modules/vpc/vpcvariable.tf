variable "vpc_cidr" {
  default = "10.20.0.0/16"
}

variable "subnets_cidr" {
  type = list(any)
}

variable "azs" {
  type = list(any)
}
