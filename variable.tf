

variable "region" {
  
  type = string
  description = "aws_region"
  default = "us-east-2"

}

variable "ami" {
  #type = string
  default = {
    
        "us-east-2"= "ami-0aeb7c931a5a61206"
        "us-east-1"="ami-09d56f8956ab235b3"
  
      }
}

variable "instance_type" {
  #type = string
  default = {
        "us-east-2"="t2.micro"
        "us-east-1"="t2.small"
  
      }
}


variable "vpc_cidr" {
  default = "10.20.0.0/16"
}

variable "subnets_cidr" {
  type = list(any)
  default = ["10.20.1.0/24"]

}

variable "azs" {
  type = list(any)
  default = [ "us-east-1a" ]
}


variable "servernames" {
  type = list(string)
  default = [ "farrukhserver1" ]

}

