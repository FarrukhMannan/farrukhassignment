# # VPC

module "vpc" {
  source = "./modules/vpc"
  vpc_cidr = var.vpc_cidr
  subnets_cidr= var.subnets_cidr
  azs=var.azs

}


module "webserver1" {
  source = "./modules/webserver"
  ami             = "${lookup(var.ami, var.region)}"
  instance_type   = "${lookup(var.instance_type, var.region)}"
  key= file("${path.module}/id_rsa.pub")
  name           = var.servernames
  subnet_id =   module.vpc.subnet.*.id 
  vpc_id = module.vpc.vpc_id
 # security_groups = aws_security_group.publicsg.id
}



 

